class Employee {
	constructor (name, age, salary){
		this.name = name;
		this.age = age;
		this.salary = salary;
	}
	//get/set of Employee
	get name(){
		return this._name;
	}
	set name(getName){
		this._name = getName;
	}

	get age(){
		return this._age;
	}
	set age(getAge){
		this._age = getAge;
	}

	get salary(){
		return this._salary;
	}
	set salary(getSalary){
		this._salary = getSalary;
	}
}

	//extend of class Employee + adding lang
class Programmer extends Employee {
	constructor(name, age, salary, lang) {
		super(name, age, salary);
		this.lang = lang;
	}

	//multiplication of salary
	get salary(){
		return this._salary * 3;
	}
	set salary(getSalary){
		this._salary = getSalary;
	}

	get lang(){
		return this._lang;
	}
	set lang(getLang){
		this._lang = getLang;
	}
}

const dataAlex = new Programmer('Alex', 38, 10200, "Ukrainian");
const dataJama = new Programmer('Jama', 42, 8600, "Spanish");

console.log(dataAlex);
console.log(dataJama);


